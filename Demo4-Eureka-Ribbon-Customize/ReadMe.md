﻿# Ribbon配置
通过自定义配置，指定某个服务使用Ribbon何种配置，例如修改某服务的策略、Ping等。

有两种方式，一种方式使用<client>.ribbon.*，另外一种使用@RibbonClient声明

## 使用@RibbonClient配置Ribbon
使用@RibbonClient注解指定哪个服务的Ribbon使用定制化配置，例如：将默认的轮询策略修改为随机策略

编码，使用Demo3，在消费服务中处理  

1、定义一个Ribbon自定义Config类，指明需要修改的Ribbon配置信息（其实是覆盖RibbonClientConfiguration中的方法）

2、在Main方法上使用@RibbonClient注解，指定哪个服务使用该Ribbon配置

注意点：如果放到Application下（即，可以被扫描到）则该项目下所有服务都会执行该配置，及该配置会被所有服务共享，如果只是想该项目下某一个服务执行该Ribbon配置，则需要将配置文件放到一个无法扫描到的地方


如何验证“注意点”呢？
我们可以做两个服务provider-user 和 provider-user2，在controller中调用，使用LoadBalancerClient选择服务，然后查看两个服务的命中情况。
/movie/test 查看

如果Config放到Application能够扫描的目录下就真无法做到单服务控制吗？

1、创建一个用于表示启动时不加载配置文件的注解 ExcludeConfig
2、在配置文件上打上该注解标识
3、在Main配置上使用excludeFilters


总结：在使用@RibbonClient注解指定服务的Ribbon配置时，必须考虑配置共享的问题，解决办法有两种：
1、将配置文件放在Application扫描不到的目录
    即将配置文件放在Application的上级目录的任意位置，确保Application启动时不会被@ComponentScan扫描到
2、配置启动加载过滤注解
    指明@ComponentScan不扫描哪个注解标注的类
    @ComponentScan(excludeFilters = {@ComponentScan.Filter(type = FilterType.ANNOTATION, value = ExcludeConfig.class)})

## 属性文件方式配置 

直接在配置文件中<client>.ribbon.*   client为服务名称，'*'号是需要配置属性，具体属性参加参考文档