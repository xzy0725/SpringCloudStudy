package com.xzy.cloud.config;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author xuzhiyuan
 * @Description
 * @Date 2018/5/11 16:18
 */
@Configuration
@ExcludeConfig
public class CustomizeRibbonConfig {
    @Bean
    public IRule ribbonRule() {
        return new RandomRule();
    }
}
