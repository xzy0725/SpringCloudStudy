package com.xzy.cloud;

//import com.xzy.config.CustomizeRibbonConfig;//必须放在与ConsumerApplication不同的目录层级，避免被扫描到，而导致所有服务Ribbon配置被覆盖
import com.xzy.cloud.config.CustomizeRibbonConfig;//如果放在能够被ComponentScann扫描到的目录，必须配置启动过滤注解
import com.xzy.cloud.config.ExcludeConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableEurekaClient
@RibbonClient(name = "provider-user", configuration = CustomizeRibbonConfig.class)
@ComponentScan(excludeFilters = {@ComponentScan.Filter(type = FilterType.ANNOTATION, value = ExcludeConfig.class)})
public class JavaConfigApplication {

	@Bean
	@LoadBalanced
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
	public static void main(String[] args) {
		SpringApplication.run(JavaConfigApplication.class, args);
	}
}
