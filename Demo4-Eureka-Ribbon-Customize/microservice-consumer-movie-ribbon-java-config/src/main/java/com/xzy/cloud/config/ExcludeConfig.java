package com.xzy.cloud.config;

/**
 * @Author xuzhiyuan
 * @Description 启动时需要过滤的Config注解
 * @Date 2018/5/14 15:08
 */
public @interface ExcludeConfig {
}
