package com.xzy.cloud.controller;

import com.xzy.cloud.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @Author xuzhiyuan
 * @Description
 * @Date 2018/5/10 10:38
 */

@RestController
public class MovieController {

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/movie/user/{id}")
    public User findById(@PathVariable Long id) {
        //此处ip地址不再使用服务的ip+端口号调用，而是使用虚拟IP(VIP)，即，Eureka Server中识别的信息
        //VIP的信息组成：spring.application.name
        return this.restTemplate.getForObject("http://provider-user/user/"+id, User.class);
    }
}
