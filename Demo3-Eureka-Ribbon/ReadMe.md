本实例测试Ribbon在Eureka层的负载均衡

1、修改程序

在Demo2的基础上，修改消费者movie,在RestTemplate Bean上增加@LoadBalanced注解

2、测试

启动多个provider-user，然后调用消费者，在多个provider-user的控制台均有打印日志信息，验证成功