package com.xzy.cloud.provider.controller;

import com.xzy.cloud.provider.entity.User;
import com.xzy.cloud.provider.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author xuzhiyuan
 * @Description
 * @Date 2018/5/10 9:55
 */

@RestController
public class UserController {
    @Autowired
    private UserRepository userRepository;

    @GetMapping("/user/{id}")
    public User findById(@PathVariable Long id){
        return this.userRepository.findOne(id);
    }
}
