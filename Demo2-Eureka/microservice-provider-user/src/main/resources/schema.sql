drop table user if exists;
CREATE TABLE user (
id bigint primary key,
USERNAME VARCHAR(32) ,
NAME VARCHAR(32),
AGE INT(4),
BALANCE DECIMAL(10,2)
)