package com.xzy.cloud.provider.repository;

import com.xzy.cloud.provider.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @Author xuzhiyuan
 * @Description
 * @Date 2018/5/10 10:09
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
}
