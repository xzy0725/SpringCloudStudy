## 实例需求：
### 1、Eureka Server

- 这是一个Eureka Server，相当于注册服务中心，用于客户端注册到该服务上
- 介绍了如何设置eureka的登录权限设置

### 2、provider-user

- 这是一个微服务，也是一个Eureka Client
- 介绍了Eureka客户端的纤细配置，包括：
    - 如何在Eureka Server中以ip注册而不是主机名注册
    - 如何设置Eureka Server中的实例名称显示
    - 如何设置Eureka Client向Server端发起心跳的周期
    - 如何设置元数据
    - 如何设置启用健康检查
    
### 3、consumer-movie

在该微服务中仅配置了Eureka，注册到Server端